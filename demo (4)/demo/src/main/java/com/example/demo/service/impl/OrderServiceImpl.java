package com.example.demo.service.impl;


import com.example.demo.domain.Order;
import com.example.demo.repository.OrderRepository;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    @Cacheable(cacheNames = "orders")
    public Page<Order> getAllOrder(Integer offset, Integer limit) {
        return orderRepository.findAll(PageRequest.of(offset,limit));
    }

    @Override
    public Page<Order> getAllOrdered(Integer offset, Integer limit) {
        return orderRepository.findAllByStatusEquals(1,PageRequest.of(offset,limit));
    }

    @Override
    public Page<Order> getAllOrdering(Integer offset, Integer limit) {
        return orderRepository.findAllByStatusEquals(0,PageRequest.of(offset,limit));
    }
}
