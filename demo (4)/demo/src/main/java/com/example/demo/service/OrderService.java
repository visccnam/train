package com.example.demo.service;

import com.example.demo.domain.Order;
import org.springframework.data.domain.Page;

public interface OrderService {
    Page<Order> getAllOrder(Integer offset, Integer limit);
    Page<Order> getAllOrdered(Integer offset, Integer limit);
    Page<Order> getAllOrdering(Integer offset, Integer limit);
}
