package com.example.demo.controller;


import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order/")
public class OrderController {

    @Autowired
    OrderService orderService;

    /*Lay danh sach tat ca cac order*/
    @GetMapping("getAllOrder")
    public ResponseEntity<?> getAllOrder(@RequestParam(defaultValue = "0") Integer offset, @RequestParam(defaultValue = "10") Integer limit){
        return ResponseEntity.ok(orderService.getAllOrder(offset,limit));
    }
    /*Lay danh sach tat ca don hang da nhap ve*/
    @GetMapping("getAllOrdered")
    public ResponseEntity<?> getAllOrdered(@RequestParam(defaultValue = "0") Integer offset, @RequestParam(defaultValue = "10") Integer limit){
        return ResponseEntity.ok(orderService.getAllOrdered(offset,limit));
    }
    /*Lay danh sach tat cac don hang se nhap*/
    @GetMapping("getAllOrdering")
    public ResponseEntity<?> getAllOrdering(@RequestParam(defaultValue = "0") Integer offset, @RequestParam(defaultValue = "10") Integer limit){
        return ResponseEntity.ok(orderService.getAllOrdering(offset,limit));
    }
}
