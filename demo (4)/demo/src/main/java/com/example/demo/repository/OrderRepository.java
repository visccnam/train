package com.example.demo.repository;

import com.example.demo.domain.Order;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    @Cacheable(cacheNames = "orderList")
    Page<Order> findAllByStatusEquals(Integer status, Pageable pageable);
}
